const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();
const router = express.Router();
const initializeRoutes = require('./routes/init');

const {errHandler} = require('./utils');

const environment = process.env.NODE_ENV;
const stage = require('./config')[environment || 'development'];


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

if ((environment || '').toUpperCase() !== 'PRODUCTION') {
    app.use(logger('dev'));
}

app.listen(stage.port, () => {
    console.log(`Serve listen on port ${stage.port}`);
});

mongoose.connect(stage.MONGODB_URL, { useNewUrlParser: true })
    .catch(err => { console.log("DATABASE CONNECTION ERROR")});

initializeRoutes(router);

app.use(router);

module.exports = app;