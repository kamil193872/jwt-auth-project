//const mongoose = require('mongoose');
const User = require('../models/users');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {errHandler} = require('../utils');

const environment = process.env.NODE_ENV;
const stage = require('../config')[environment];

module.exports = {
    get: (req, res) => {
        User.find({}).then(docs => {
                res.json(docs);
            })
        .catch(err => { errHandler(err, 202, res) })

    },
    add: (req, res) => {
        const {name, password} = req.body;
        const user = new User({name, password});

        return user.save().then(user => {
                    res.status(200).send(user);
                })
        .catch(err => { errHandler(err, 202, res) });
    },
    login: (req, res) => {
        const {name, password} = req.body;

        return User.findOne({name}).then(user => {
            if (!user) throw new Error("No User found");

            return bcrypt.compare(password, user.password).then(isMatching => {
                    let token = null;
                    if (isMatching) {
                        const payload = {phrase: 'gitara siema', user, admin: true};
                        const options = {expiresIn: '2d', issuer: 'localhost'};
                        token = jwt.sign(payload, stage.JWT_SECRET, options)
                    }

                    res.send(`isMaching: ${isMatching} \n token: ${token}`);
                })
        })
        .catch(err => { errHandler(err, 202, res) });
    }
}