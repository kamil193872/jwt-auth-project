module.exports = {
    development: {
        port: process.env.PORT || 3000,
        MONGODB_URL: 'mongodb://***:****@**.***.***.**:27017/***_db',
        SALT_ROUNDS: 4,
        JWT_SECRET: '*****'
    }
}