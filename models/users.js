const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Schema = mongoose.Schema;

const environment = process.env.NODE_ENV;
const stage = require('../config')[environment];

const userSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    }
});

userSchema.pre('save', function (next) {
   const user = this;

   if (!user.isModified() || !user.isNew) {
       next();
   } else {
       bcrypt.hash(user.password, stage.SALT_ROUNDS).then(hash => {
           user.password = hash;
           next();
       })
       .catch(err => {
           console.log('ERROR hashing password', err);
           next();
       })
   }
});

module.exports = mongoose.model('User', userSchema);