const usersController = require('../controllers/users');
const {validateToken} = require('../utils');

module.exports = (router) => {
    router.route('/users')
        .get(validateToken, usersController.get)
        .post(usersController.add);

    router.route('/login')
        .post(usersController.login);
}