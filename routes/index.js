const controller = require('../controllers/index');

module.exports = (router) => {
    router
        .route('/')
        .get(controller.main);
}