const mainRoute = require('../routes/index');
const usersRoute = require('../routes/users');

module.exports = (router) => {
    mainRoute(router);
    usersRoute(router);


    router.use('/api/v1', (req, res, next) => {
        res.send('Gitara siema');
        next();
    });

}