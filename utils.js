const jwt = require('jsonwebtoken');
const environment = process.env.NODE_ENV;
const stage = require('./config')[environment];

module.exports = {
    errHandler: (errObj, statusCode, resObj) => {
        const isErr = !!errObj;
        if (isErr) {

        }
        return isErr;
    },
    validateToken : (req, res, next) => {
        const {token} = req.headers;

        try {
            const options = {expiresIn: '2d', issuer: 'localhost'};
            jwt.verify(token, stage.JWT_SECRET, (err, decoded) => {
                if (!!err) throw new Error('Invalid Token');

                res.set('auth', true);
                res.set('xx', decoded);

                next();
            })
        } catch (err) {
            res.status(202).send(err.toString());
        }

    }
}